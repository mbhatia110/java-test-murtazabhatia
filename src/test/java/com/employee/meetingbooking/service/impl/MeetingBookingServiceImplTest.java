package com.employee.meetingbooking.service.impl;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.employee.meetingbooking.exception.MeetingBookingException;
import com.employee.meetingbooking.model.MeetingsBooking;

@ExtendWith(MockitoExtension.class)
class MeetingBookingServiceImplTest {

	@InjectMocks
	MeetingBookingServiceImpl meetingBookingServiceImpl;

	@Test
	void shouldProcessBookingRequest() throws Exception {
		String request = "0900 1730\n" + "2020-01-18 10:17:06 EMP001\n" + "2020-01-21 09:00 2\n"
				+ "2020-01-18 12:34:56 EMP002\n" + "2020-01-21 09:00 2\n" + "2020-01-18 09:28:23 EMP003\n"
				+ "2020-01-22 14:00 2\n" + "2020-01-18 11:23:45 EMP004\n" + "2020-01-22 16:00 1\n"
				+ "2020-01-15 17:29:12 EMP005\n" + "2020-01-21 16:00 3\n" + "2020-01-18 11:00:45 EMP006\n"
				+ "2020-01-23 16:00 1\n" + "2020-01-15 11:00:45 EMP007\n" + "2020-01-23 15:00 2";

		List<MeetingsBooking> actual = meetingBookingServiceImpl.booking(request);

		Assertions.assertNotNull(actual);
		Assertions.assertEquals(3, actual.size());
		Assertions.assertEquals("EMP001", actual.get(0).getBookings().get(0).getEmpId());
		Assertions.assertEquals("EMP003", actual.get(1).getBookings().get(0).getEmpId());
		Assertions.assertEquals("EMP004", actual.get(1).getBookings().get(1).getEmpId());
		Assertions.assertEquals("EMP007", actual.get(2).getBookings().get(0).getEmpId());
	}

	@Test
	void shouldThrowExceptionForProcessBookingRequest() throws Exception {
		String request = " ";

		Assertions.assertThrows(MeetingBookingException.class, () -> {
			meetingBookingServiceImpl.booking(request);
		});
	}

	@Test
	void shouldThrowExceptionForProcessBookingRequestWhenSomethingWentWrong() throws Exception {
		String request = "asdasdasd";

		Assertions.assertThrows(MeetingBookingException.class, () -> {
			meetingBookingServiceImpl.booking(request);
		});
	}

}
