package com.employee.meetingbooking.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.employee.meetingbooking.model.Bookings;
import com.employee.meetingbooking.model.MeetingsBooking;
import com.employee.meetingbooking.service.MeetingBookingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MeetingsBookingController.class)
@AutoConfigureMockMvc(addFilters = false)
class MeetingsBookingControllerTest {

	@MockBean
	MeetingBookingService meetingBookingService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void shouldPing() throws Exception {
		mockMvc.perform(get("/employee/meeting/ping")).andExpect(status().isOk());
	}

	@Test
	void shouldProcessBookingRequest() throws Exception {

		String request = "0900 1730\n" + "2020-01-18 10:17:06 EMP001\n" + "2020-01-21 09:00 2\n"
				+ "2020-01-18 12:34:56 EMP002\n" + "2020-01-21 09:00 2\n";

		MeetingsBooking meetingsBooking = new MeetingsBooking();

		meetingsBooking.setDate("2020-01-21");

		Bookings bookings = new Bookings("EMP001", "09:00", "11:00");

		List<Bookings> bookingList = new ArrayList<>();
		bookingList.add(bookings);

		meetingsBooking.setBookings(bookingList);

		List<MeetingsBooking> meetingsBookings = new ArrayList<>();
		meetingsBookings.add(meetingsBooking);

		Mockito.when(meetingBookingService.booking(Mockito.anyString())).thenReturn(meetingsBookings);

		mockMvc.perform(post("/employee/meeting/bookingrequest").content(new ObjectMapper().writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].date", is("2020-01-21")))
				.andExpect(jsonPath("$.[0].bookings.[0].emp_id", is("EMP001")));

	}

}
