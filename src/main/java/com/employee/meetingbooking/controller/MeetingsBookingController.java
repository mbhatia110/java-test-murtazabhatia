package com.employee.meetingbooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.meetingbooking.model.MeetingsBooking;
import com.employee.meetingbooking.service.MeetingBookingService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/employee/meeting")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MeetingsBookingController {

	private final MeetingBookingService meetingBookingService;

	@GetMapping(path = "/ping", produces = "application/json")
	public String ping() {
		String response = "running";

		return ResponseEntity.ok(response).getBody();
	}

	@PostMapping(path = "/bookingrequest")
	public ResponseEntity<List<MeetingsBooking>> processBookingRequest(@RequestBody String meetingBookingRequest) {

		return ResponseEntity.ok(meetingBookingService.booking(meetingBookingRequest));
	}

}
