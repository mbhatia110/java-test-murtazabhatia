package com.employee.meetingbooking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MeetingBookingException extends RuntimeException {

	private static final long serialVersionUID = 8753430744554301586L;

	public MeetingBookingException(String errorMessage) {
		super(errorMessage);
	}

	public MeetingBookingException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
	}

}
