package com.employee.meetingbooking.exception;

import lombok.Data;

/**
 * Represents error response returned in case of managed exceptions.
 * 
 */
@Data
public class ExceptionResponse {
	private String errorCode;
	private String errorMessage;
	private String url;
	private int httpResponseCode;

	public ExceptionResponse(String errorCode, String errorMessage, String url, int httpResponseCode) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.url = url;
		this.httpResponseCode = httpResponseCode;
	}

}