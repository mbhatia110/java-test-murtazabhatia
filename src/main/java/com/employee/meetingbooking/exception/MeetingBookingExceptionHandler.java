package com.employee.meetingbooking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Provides exception handling across application.
 * 
 */
@ControllerAdvice
@RestController
public class MeetingBookingExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllExceptions(Exception ex, WebRequest request) {
		return response(HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MeetingBookingException.class)
	public final ResponseEntity<ExceptionResponse> handleInternalServerError(MeetingBookingException ex,
			WebRequest request) {
		return response("500", ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<ExceptionResponse> response(String errorCode, Exception ex, WebRequest request,
			HttpStatus status) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(errorCode, ex.getMessage(),
				request.getDescription(false), status.value());
		return new ResponseEntity<>(exceptionResponse, status);

	}

}