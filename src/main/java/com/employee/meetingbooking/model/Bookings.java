package com.employee.meetingbooking.model;

import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NonNull;

@Data
public class Bookings implements Comparable<Bookings> {

	private static final String HH_MM = "HH:mm";

	@NonNull
	@JsonProperty("emp_id")
	private final String empId;

	@NonNull
	@JsonProperty("start_time")
	private final String startTime;

	@NonNull
	@JsonProperty("end_time")
	private final String endTime;

	@JsonIgnore
	private LocalDateTime requestTime;

	@Override
	public int compareTo(Bookings o) {
		Interval meetingInterval = new Interval(
				DateTimeFormat.forPattern(HH_MM).parseLocalTime(startTime).toDateTimeToday(),
				DateTimeFormat.forPattern(HH_MM).parseLocalTime(endTime).toDateTimeToday());
		Interval toCompareMeetingInterval = new Interval(
				DateTimeFormat.forPattern(HH_MM).parseLocalTime(o.getStartTime()).toDateTimeToday(),
				DateTimeFormat.forPattern(HH_MM).parseLocalTime(o.getEndTime()).toDateTimeToday());

		if (meetingInterval.overlaps(toCompareMeetingInterval)) {
			return 0;
		} else {
			return this.getStartTime().compareTo(o.getStartTime());
		}
	}

}
