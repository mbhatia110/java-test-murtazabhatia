package com.employee.meetingbooking.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeetingsBooking {

	private String date;

	private List<Bookings> bookings;

}
