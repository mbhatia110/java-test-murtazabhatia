package com.employee.meetingbooking.service;

import java.util.List;

import com.employee.meetingbooking.model.MeetingsBooking;

public interface MeetingBookingService {

	List<MeetingsBooking> booking(String meetingBookingRequest);

}
