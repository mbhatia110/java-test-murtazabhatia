package com.employee.meetingbooking.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.employee.meetingbooking.exception.MeetingBookingException;
import com.employee.meetingbooking.model.Bookings;
import com.employee.meetingbooking.model.MeetingsBooking;
import com.employee.meetingbooking.service.MeetingBookingService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MeetingBookingServiceImpl implements MeetingBookingService {

	private static final String T = "T";
	private static final String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String HH_MM = "HH:mm";
	private static final String YYYY_MM_DD = "yyyy-MM-dd";
	private static final String NEWLINE = "\n";
	private static final String SPACE = " ";

	/**
	 * Process the requested meeting booking by employee and return the list of
	 * booking based on the rules below
	 * 
	 * Booking rules 
	 * - No part of a meeting may fall outside of office hours 
	 * - Meetings may not overlap. 
	 * - The booking submission system only allows one submission at a time, so submission times are guaranteed to be unique. 
	 * - Booking must be processed in the chronological order in which they were submitted. 
	 * - The ordering of booking submissions in the supplied input is not guaranteed.
	 * 
	 * @param meetingBookingRequest
	 * 
	 * @throws MeetingBookingException
	 * @return List<MeetingsBooking>
	 */
	@Override
	public List<MeetingsBooking> booking(String meetingBookingRequest) {

		try {
			if (StringUtils.isBlank(meetingBookingRequest)) {
				throw new MeetingBookingException("Employee has requested for booking is not a valid input");
			}

			String[] requestLines = meetingBookingRequest.split(NEWLINE);

			String[] officeHours = requestLines[0].split(SPACE);
			LocalTime officeStartTime = new LocalTime(Integer.parseInt(officeHours[0].substring(0, 2)),
					Integer.parseInt(officeHours[0].substring(2, 4)));
			LocalTime officeFinishTime = new LocalTime(Integer.parseInt(officeHours[1].substring(0, 2)),
					Integer.parseInt(officeHours[1].substring(2, 4)));

			Map<LocalDate, Set<Bookings>> meetings = new HashMap<>();

			for (int i = 1; i < requestLines.length; i = i + 2) {

				String[] meetingSlotRequest = requestLines[i + 1].split(SPACE);
				LocalDate meetingDate = DateTimeFormat.forPattern(YYYY_MM_DD).parseLocalDate(meetingSlotRequest[0]);

				Bookings meeting = extractMeetingBooking(requestLines[i], officeStartTime, officeFinishTime,
						meetingSlotRequest);
				if (meeting != null) {
					if (meetings.containsKey(meetingDate)) {
						meetings.get(meetingDate).removeIf(a -> a.getRequestTime().isAfter(meeting.getRequestTime()));
						meetings.get(meetingDate).add(meeting);
					} else {
						Set<Bookings> meetingsForDay = new TreeSet<>();
						meetingsForDay.add(meeting);
						meetings.put(meetingDate, meetingsForDay);
					}
				}
			}

			return constructMeetingsBooking(meetings);
		} catch (MeetingBookingException mbe) {
			log.error(mbe.getMessage());
			throw mbe;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new MeetingBookingException("Something went wrong with MeetingBookingApp", e);
		}
	}

	/**
	 * Prepare the final output for the requested meeting bookings
	 * 
	 * @param meetings
	 * @return
	 */
	private List<MeetingsBooking> constructMeetingsBooking(Map<LocalDate, Set<Bookings>> meetings) {

		Map<LocalDate, Set<Bookings>> sortedMeeting = meetings.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
						LinkedHashMap::new));

		List<MeetingsBooking> meetingBookingList = new ArrayList<>();

		sortedMeeting.entrySet().stream().forEach(meetingMap -> {

			var meetingBookings = new MeetingsBooking();
			meetingBookings.setDate(meetingMap.getKey().toString());

			Set<Bookings> bookings = meetingMap.getValue();

			List<Bookings> bookingList = new ArrayList<>();
			for (Bookings booking : bookings) {
				var newBooking = new Bookings(booking.getEmpId(), booking.getStartTime(), booking.getEndTime());
				bookingList.add(newBooking);
			}

			meetingBookings.setBookings(bookingList);

			meetingBookingList.add(meetingBookings);
		});

		return meetingBookingList;
	}

	/**
	 * Check if the meeting falls outside of office hours
	 * 
	 * @param officeStartTime
	 * @param officeFinishTime
	 * @param meetingStartTime
	 * @param meetingFinishTime
	 * @return
	 */
	private boolean meetingBookingOutsideOfficeHours(LocalTime officeStartTime, LocalTime officeFinishTime,
			LocalTime meetingStartTime, LocalTime meetingFinishTime) {
		return meetingStartTime.isBefore(officeStartTime) || meetingStartTime.isAfter(officeFinishTime)
				|| meetingFinishTime.isAfter(officeFinishTime) || meetingFinishTime.isBefore(officeStartTime);
	}

	/**
	 * Extract the requested meeting and check if it falls outside of office hours
	 * 
	 * @param requestLine
	 * @param officeStartTime
	 * @param officeFinishTime
	 * @param meetingSlotRequest
	 * @return
	 */
	private Bookings extractMeetingBooking(String requestLine, LocalTime officeStartTime, LocalTime officeFinishTime,
			String[] meetingSlotRequest) {
		String[] employeeRequest = requestLine.split(SPACE);
		String employeeId = employeeRequest[2];

		LocalTime meetingStartTime = DateTimeFormat.forPattern(HH_MM).parseLocalTime(meetingSlotRequest[1]);
		LocalTime meetingFinishTime = new LocalTime(meetingStartTime.getHourOfDay(), meetingStartTime.getMinuteOfHour())
				.plusHours(Integer.parseInt(meetingSlotRequest[2]));

		if (meetingBookingOutsideOfficeHours(officeStartTime, officeFinishTime, meetingStartTime, meetingFinishTime)) {
			log.error("EmployeeId:: {} has requested booking which is outside office hour.", employeeId);
			return null;
		} else {
			var newBooking = new Bookings(employeeId, meetingStartTime.toString(HH_MM),
					meetingFinishTime.toString(HH_MM));
			newBooking.setRequestTime(DateTimeFormat.forPattern(YYYY_MM_DD_T_HH_MM_SS)
					.parseLocalDateTime(employeeRequest[0] + T + employeeRequest[1]));
			return newBooking;

		}
	}

}
